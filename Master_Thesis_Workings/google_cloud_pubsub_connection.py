import signal
import os,sys
from google.cloud import pubsub_v1
from concurrent.futures import TimeoutError
import time

os.chdir("D:/Git/evaluation/MQTT_prototype/data")

credentials_path = ''
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credentials_path
timeout = 10.0
project_id = 'rr-iot'
publisher = pubsub_v1.PublisherClient()
subscriber = pubsub_v1.SubscriberClient()

topic_id = "mqtt_gamma2"
topic_path = publisher.topic_path(project_id, topic_id)
subscription_path = 'projects/rr-iot/subscriptions/mqtt_gamma2-sub2'


def publish_file(filename,chunk_size):
    start = time.time() #Publish time
    i=0
    myfile = open(filename, "r")
    
    while myfile:
        chunk = myfile.read(chunk_size)
        
        attributes = {
            'Device_Serial_No': 'R2_Gamma_2_001',
        }
        
        print(chunk)
        
        if chunk == "":
            break
        future = publisher.publish(topic_path, chunk.encode("ISO-8859-1"))
        print(f'published message id {future.result()}')
        print("File transfer is done ")
    myfile.close()

    time_taken=time.time()-start

    print("[GCPS] File Transfer is done. Time taken :",time_taken)
    return

def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)
"""
def callback(message):
        filename  = "Default.txt"
        print(f'Received message: {message}')
        print(f'data: {message.data}')
        sub_data = message.data
        
        print (sub_data)

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
    print('Received message:', message)
    #print('data: ',message.data)
    new_file = open(filename,"a")
    new_file.write(str(sub_data.decode("ISO-8859-1")) )
        #time.sleep(2)
    message.ack()  
        #print("Acknowledged")   
"""        
        #new_file.close
        #print("File Closed")
        
        #streaming_pull_future.cancel()
        #test#
        #if message.attributes:
        #    print("Attributes:")
        #    for key in message.attributes:
        #        value = message.attributes.get(key)
        #        print(f"{key}: {value}")
        #sys.exit(0)
        #signal.signal(signal.SIGINT, signal_handler)
        #print("b4 done")
        #return "Done"



def subscribe_file(filename):

    print ("subfile = " + filename)   
    def callback(message: pubsub_v1.subscriber.message.Message) -> None:
        #print('Received message:', message)
        #print('data: ',message.data)
        sub_data = message.data
        new_file = open(filename,"a")
        new_file.write(str(sub_data.decode("ISO-8859-1")) )
        time.sleep(0.5)
        new_file.close()
        print("sub Callback Done")  
        message.ack()      
    
    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
    print(f'Listening for messages on {subscription_path}..\n')

    
    with subscriber:                                                # wrap subscriber in a 'with' block to automatically call close() when done
        try:
            #Establishes a stream with the server, which sends messages down to the client.
            #streaming_pull_future.result(timeout=timeout)
            streaming_pull_future.result(timeout=timeout)
            print("subscriber Done")
            
            #streaming_pull_future.cancel()
            #return# going without a timeout will wait & block indefinitely
        except TimeoutError:
            streaming_pull_future.cancel()                          # trigger the shutdown
            streaming_pull_future.result()                          # block until the shutdown is complete
            print("Error : Timeout")
            #exit()
        except KeyboardInterrupt:
            print('Interrupted')
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
    return 