import socket, struct,time
import os 

SOCKET_HOST = "0.0.0.0"
#SOCKET_HOST = socket.gethostbyname(socket.gethostname())
SOCKET_PORT = 5673
FORMAT = "ISO-8859-1"

os.chdir("/home/jacky/Git/evaluation/MQTT_prototype/data")


global conn_flag 
sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)


def send_data(conn, data):
    size = len(data)
    print("size_in_4_bytes :",size)
    if size <= 0:
        print("b''")
        return
    else: 
        size_in_4_bytes = struct.pack('I', size)
        print("size_in_4_bytes sent:",size_in_4_bytes)
        conn.send(size_in_4_bytes)
        
        conn.send(data.encode(FORMAT))
        
def recv_data(conn):
    size_in_4_bytes = conn.recv(4)
    print("size_in_4_bytes recived :",size_in_4_bytes)
    if not size_in_4_bytes:
        #size_in_4_bytes == b'':
        print("b'', no more data")
        return
    else:
        size = struct.unpack('I', size_in_4_bytes)
        size = size[0]
        
        print("size_in_4_bytes :",size)
        data = conn.recv(size).decode(FORMAT)
    return data

def client_file_transfer(file_directory,chunksize,broker, port):
        sock_client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        #sock_client.connect(("192.168.50.193",9896))
        sock_client.connect((broker,port))        
        start=time.time()        
        
        
        #sock.send(file_directory.encode(FORMAT))
        #msg = sock.recv(SIZE).decode(FORMAT)
        #print(f"[SERVER]: {msg}")
        with open (file_directory, "r") as file:
            while True:
                data = file.read(chunksize)
                if not data: print("Break. End of file");break
                send_data(sock_client,data)
                time.sleep(1)
            #https://docs.python.org/3/library/socket.html
            #https://code.activestate.com/recipes/408859/
            
            #print(f"[SERVER]: {msg}")
            #print(sock.recv(SIZE).decode(FORMAT))
            
        
        
        sock_client.shutdown(2)
        sock_client.close()
        print("Close File Transfer Socket")
                          
        time_taken=time.time()-start
        print("[SOCKET] File is transferred in ",time_taken, "seconds. Getting disconnect ...")
        return


def accept_client(new_file):
    print("Listening")
    conn,addr = sock.accept()
     #   conn_flag = True
    
    
    with conn:
        #print(f"[NEW CONNECTION] {addr} connected.")
        
        #file = open(new_file, "w")
        with open(new_file, "w") as file:
            while True:
                data = recv_data(conn)
                if not data : 
                    print("No more data received")
                    conn_flag = False
                    break
                file.write(data)
                print("File Receiving...")
        
        conn.close()
    print("End Conn Flag ",str(conn_flag))
    return 
    
    
def start_server():#recv_type):
    #conn_flag = False
    #sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # solution for "[Error 89] Address already in use". Use before bind()
    sock.bind((SOCKET_HOST,SOCKET_PORT))
    sock.listen(3) #waits for the client to approach the server to make a connection
    print ('Started Socker Server on',SOCKET_PORT)
    #print("recv_type=",recv_type)
    
    #if conn_flag == False:
    return sock

     



""" ***** Local Socket Testing *****
def start_client():
    print("0.0.0.0")
    print(SOCKET_PORT)
    sock_client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock_client.connect(("0.0.0.0",9896))
    print ('Connect to server',SOCKET_HOST,SOCKET_PORT)

if __name__=='__main__':
    file_directory = "2022-06-28T10-37-53_machine.csv"

    #SOCKET_HOST = ""
    SOCKET_HOST = "0.0.0.0"
    #SOCKET_HOST = socket.gethostbyname(socket.gethostname())
    SOCKET_PORT = 9896
    FORMAT = "ISO-8859-1"
    SIZE = 1024*2048
    
    os.chdir("/home/jacky/Git/evaluation/MQTT_prototype/data")
    
    sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.connect((SOCKET_HOST,SOCKET_PORT))
    file_transfer(file_directory)    
"""
