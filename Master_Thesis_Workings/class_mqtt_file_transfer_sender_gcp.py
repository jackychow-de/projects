#import paho.mqtt.publish as mqtt_publish
import paho.mqtt.client as mqtt
import os, math, json, re, time
#import socket_connection as socket
import google_cloud_pubsub_connection as gcps

import logging


os.chdir("C:/Users/Administrator/Desktop/MQTT_prototype/data")

logging.basicConfig(handlers=[logging.FileHandler(filename="./google_cloud_1mb_300mb_mqtt_log_sender.log", 
                                                 encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s", 
#                    datefmt="%F %A %T", log_records_send
                    level=logging.INFO)


pattern = re.compile(".*[+#].*")
mqtt_auth = { 'username': 'rr', 'password': 'rr2015' }
key = ""
key = "T866"
#while bool(pattern.match(key)) or key == "":   
#	key = input("Enter a name for assiging MQTT topics (without # or +): ")
#	if key == "": 
#		print("Please enter a non empty string")
  
conn_method = ""
conn_method = "mqtt"

#while (conn_method != "mqtt" and conn_method != "socket" and conn_method != "gcps" ) or conn_method  == "":
#    conn_method = input("Enter a file streaming method (mqtt / socket /gcps) ") 

#filename = "2022-06-28T10-37-53_machine.csv"
#filename = "2021-02-18T14-26-01.log"
filename ="300mb_machine.csv" 
#filename = "Xmb_machine_.csv"

while filename == "":
    filename= input("Enter filename with extension: ")

sender_adr = ""
sender_port = 9896

#broker = "91.41.89.1" #Broker Ip  address
broker = "0.0.0.0"
port = 1884
keep_alive = 6000 #Connection between publisher and broker keeps alive file_openr 60 seconds,
#it will be disconencted automatically if no datat is transfer within this time interval


start_time = ""
end_time = ""


filesize = round(os.stat(filename).st_size / 1024.0, 2)
chunksize = 1024 *1024

chunkcount = math.ceil(os.stat(filename).st_size / chunksize)

class Send():
    def __init__(self):
            super(Send, self).__init__()
            self.fileinfo_Topic = "/{0}/fileinfo".format(key)
           
            self.mqtt_fileTRF_Topic = "/{0}/mqtt_TRF".format(key)
            self.socket_fileTRF_Topic = "/{0}/socketfileTRF".format(key)
            self.gcps_fileTRF_Topic = "/{0}/gcpsfileTRF".format(key)

            self.senderChannel_Topic = "/{0}/senderChannel".format(key)
            self.receiverChannel_Topic = "/{0}/receiverChannel".format(key)

            self.start_time =0
            self.end_time =0
            self.counter =0
            self.msg_send_time =0
            self.buffer = 1000

            self.mqttc=mqtt.Client("MQTT_Publish",clean_session=True)
            self.mqttc.username_pw_set('', '')
            
            self.mqttc.on_subscribe=self.on_subscribe
            self.mqttc.on_connect=self.on_connect
            #self.mqttc.on_message=self.on_message

            self.mqttc.connect(broker,port,keep_alive)
            self.mqttc.loop_start() 

    def connection(self):
       
        #self.mqttc.publish(self.senderChannel_Topic, payload="0", qos=1 )
        
        #self.file_info_transfer()
        time.sleep(10)
        self.start_time = time.time() 
        self.google_cloud_pub_sub_file_transfer()
        #self.mqtt_file_transfer()
        

               

   
    def on_connect(self, mqttc,userdata, flags, rc):
        
        self.mqttc.subscribe(self.receiverChannel_Topic)
        
        print("Waiting for the Server to connect...")
        if rc ==0:
            print("Connected to MQTT Broker")
            
        else:
            print('Failed to connect, return code ({0})'.format(rc))
            self.mqttc.reconnect()

    def on_subscribe(self,mosq, obj,mid,granted_qos):
        print("Subscribed" +str(mid)+ " "+ str(granted_qos))

    def google_cloud_pub_sub_file_transfer(self):
        print("Run GCP IOT Core")
        self.mqttc.publish(self.gcps_fileTRF_Topic, "1",qos=1)
        gcps.publish_file(filename,chunksize)
        print("[GCPS file published ]")
        self.mqttc.publish(self.senderChannel_Topic, "1",qos=1)
        

    def socket_file_transfer(self):
        self.mqttc.publish(self.socket_fileTRF_Topic, "1",qos=1)
        #socket.client_file_transfer(filename,chunksize,sender_adr ,sender_port)
        self.mqttc.publish(self.senderChannel_Topic, "1",qos=1)

    def disconnect(self):
        self.end_time = time.time()
        self.time_taken= self.end_time - self.start_time
        print("File transfer sucessful. Time taken: {0} seconds" .format(self.time_taken))
        logging.info("File transfer sucessful. Time taken: {0} seconds" .format(self.time_taken))
        self.mqttc.loop_stop()
        exit()

    def mqtt_file_transfer(self):
 
        f=open(filename, "r") 
        print("File opened.")
        print("Starting file data transmission via MQTT")
        fileContent = f.read(chunksize)
        while(fileContent):
            byteArr = (fileContent.encode("ISO-8859-1"))
            #print(byteArr)
            self.msg_send_time =time.time()
            print("\tSending chunk ({0})...".format(self.counter))
            self.mqttc.publish(self.mqtt_fileTRF_Topic, payload=byteArr, qos=1)
            
            logging.info("Messsage No ({0}) with start time ({1}) ".format(self.counter,self.msg_send_time))
            self.counter += 1
            #time.sleep(1) #1mb
            #time.sleep(0.1) # 0.1mb
            #time.sleep(0.01) #0.01mb
            #rest = self.buffer - self.counter
            """
            if self.counter == self.buffer : #temp rest per 1000 msg
                time.sleep(25)
                self.buffer += 1000
                print ("Buffer : ".format(self.buffer))
            """
            fileContent = f.read(chunksize)
            

        print("File sent successfully!")
        self.mqttc.publish(self.senderChannel_Topic, payload="1",qos=1)

        if fileContent =="":          
            self.disconnect()
            


if __name__ == "__main__":
    try:
        mqtt_txf = Send()
        mqtt_txf.connection()
  
        
    except KeyboardInterrupt:
        pass