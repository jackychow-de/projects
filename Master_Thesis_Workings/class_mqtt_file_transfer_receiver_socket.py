import os
import socket_connection as socket

import logging


dirname = os.path.dirname(__file__)


os.chdir(dirname)

logging.basicConfig(handlers=[logging.FileHandler(filename="./P2_1cf_socket_log_records_rec.log", 
                                                 encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s", 
#                    datefmt="%F %A %T", 
                    level=logging.INFO)


filepath = os.path.join(dirname, '_uploaded_')
os.chdir(filepath)


class Recv():
    def __init__(self):
        super(Recv, self).__init__()
           
        self.filename = "300mb_machine.csv"
        self.chunks = 0
        self.file = None
        self.check_connect = False
        self.counter = 0		

        self.start_time =0
        self.stop_time=0
        self.elapsed_time=0
        self.msg_rev_time=0
  
    def connection(self): 
        socket.start_server()
             
        socket.accept_client(self.filename)

if __name__ == "__main__":
    try:
        mqtt_rec = Recv()
        mqtt_rec.connection()

    except KeyboardInterrupt:
        pass
	