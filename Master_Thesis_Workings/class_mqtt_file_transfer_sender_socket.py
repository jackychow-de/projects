import os, math, json, re, time
import socket_connection as socket
import logging


os.chdir("/home/jacky/Git/evaluation/MQTT_prototype/data")

logging.basicConfig(handlers=[logging.FileHandler(filename="./Socket_1mb_300mb_mqtt_log_sender.log", 
                                                 encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s", 
#                    datefmt="%F %A %T", log_records_send
                    level=logging.INFO)


filename ="300mb_machine.csv" 

while filename == "":
    filename= input("Enter filename with extension: ")

sender_adr = ""
sender_port = 5673

start_time = ""
end_time = ""

filesize = round(os.stat(filename).st_size / 1024.0, 2)
chunksize = 1024 *1024*100

chunkcount = math.ceil(os.stat(filename).st_size / chunksize)

class Send():
    def __init__(self):
            super(Send, self).__init__()
            self.start_time =0
            self.end_time =0
            self.counter =0
            self.chunks = chunksize/1024
            self.elapsed_time=0
            self.msg_send_time =0
            self.buffer = 1000

    def connection(self):
        self.start_time = time.time()
        socket.client_file_transfer(filename,chunksize,sender_adr ,sender_port)
        self.stop_time = time.time()
        self.elapsed_time = self.stop_time- self.start_time
        logging.info("File transmit complete ({0}) with message size ({1}) MB ".format(self.elapsed_time,self.chunks))
  

if __name__ == "__main__":
    try:
        mqtt_txf = Send()
        mqtt_txf.connection()
  
        
    except KeyboardInterrupt:
        pass