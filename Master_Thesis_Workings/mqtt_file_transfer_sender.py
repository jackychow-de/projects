import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import os, math, json, re, time
import socket_connection as socket
import google_cloud_pubsub_connection as gcps

pattern = re.compile(".*[+#].*")
mqtt_auth = { 'username': '', 'password': '' }
key = ""
key = "A1234"
#while bool(pattern.match(key)) or key == "":   
#	key = input("Enter a name for assiging MQTT topics (without # or +): ")
#	if key == "": 
#		print("Please enter a non empty string")
  
conn_method = ""
conn_method = "gcps"

#while (conn_method != "mqtt" and conn_method != "socket" and conn_method != "gcps" ) or conn_method  == "":
#    conn_method = input("Enter a file streaming method (mqtt / socket /gcps) ") 

filename ="300mb_machine.csv"

while filename == "":
    filename= input("Enter filename with extension: ")

os.chdir("D:/Git/evaluation/MQTT_prototype/data")

sender_adr = ""
sender_port = 5673

broker = "" #Broker Ip  address
port = 1884
keep_alive = 60 #Connection between publisher and broker keeps alive file_openr 60 seconds,
#it will be disconencted automatically if no datat is transfer within this time interval

fileinfo_Topic = "/{0}/fileinfo".format(key)

mqtt_fileTRF_Topic = "/{0}/mqttfileTRF".format(key)
socket_fileTRF_Topic = "/{0}/socketfileTRF".format(key)
gcps_fileTRF_Topic = "/{0}/gcpsfileTRF".format(key)

senderChannel_Topic = "/{0}/senderChannel".format(key)
receiverChannel_Topic = "/{0}/receiverChannel".format(key)

start_time = ""
end_time = ""


filesize = round(os.stat(filename).st_size / 1024.0, 2)
chunksize = 1024 *1024
chunkcount = math.ceil(os.stat(filename).st_size / chunksize)

def on_connect(client, userdata, flags, rc):
    client.subscribe(receiverChannel_Topic)
    print("Waiting for the Server to connect...")

def on_message(client, userdata, message):
    global start_time
    global end_time
    
    if message.topic == receiverChannel_Topic:
        msg = str(message.payload.decode("utf-8","ignore"))
        if msg == "0":
            print("Server is connected.")
            mqtt_publish(senderChannel_Topic, "0")

            start_time = time.time()
            file_info_transfer()
            time.sleep(0.5)
            if conn_method =="mqtt":
            #--mqtt stream file
                mqtt_file_transfer()
            
            elif conn_method =="socket": 
            #---socket stream file              
                socket_file_transfer()
                
            elif conn_method =="gcps": 
            #---google_cloud_pub stream file
                google_cloud_pub_sub_file_transfer()
                 
        elif msg == "1":
            end_time = time.time()
            time_taken= end_time - start_time
            print("File transfer sucessful. Time taken: " , time_taken," seconds")
        
            exit()

def mqtt_publish(topic,msg):
    publish.single(topic, msg, qos=1 , hostname=broker, auth=mqtt_auth)
    return
    
def file_info_transfer():
    fileinfo = {}
    
    fileinfo["name"] = os.path.basename(filename)
    fileinfo["size"] = filesize
    fileinfo["chunks"] = chunkcount
    fileinfo["chunksize"] = chunksize
    fileinfo["connmethod"] = conn_method
    
    print("Sending file info...")
    
    mqtt_publish(fileinfo_Topic, json.dumps(fileinfo))
    return filename

def google_cloud_pub_sub_file_transfer():
    mqtt_publish(gcps_fileTRF_Topic, "1")
    gcps.publish_file(filename,chunksize)
    print("[GCPS file published ]")
    mqtt_publish(senderChannel_Topic, "1")
    

def socket_file_transfer():
    mqtt_publish(socket_fileTRF_Topic, "1")
    socket.client_file_transfer(filename,chunksize,sender_adr ,sender_port)
    mqtt_publish(senderChannel_Topic, "1")

def mqtt_file_transfer():
    counter = 1
    f=open(filename, "r") 
    print("File opened.")
    print("Starting file data transmission via MQTT")
    fileContent = f.read(chunksize)
    while(fileContent):
        byteArr = (fileContent.encode("ISO-8859-1"))

        print("\tSending chunk ({0})...".format(counter))
        mqtt_publish(mqtt_fileTRF_Topic, byteArr)    
        fileContent = f.read(chunksize)
        counter += 1

    print("File sent successfully!")
    mqtt_publish(senderChannel_Topic, "1")
    print("Waiting for receiver Acknowledge...")

mqttc=mqtt.Client()
mqttc.username_pw_set('rr', 'rr2015')

mqttc.on_connect=on_connect
mqttc.on_message=on_message


mqttc.connect(broker,port,keep_alive)
mqttc.loop_forever()