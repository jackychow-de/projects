# projects



## Hand_gesture_control_main
This project is to make use of Kinect v1 to capture and recognize the human gesture for transmiiting the right command.

Tag: OpenCV, Cpp, libfreenect


## Master Thesis Workings
This IOT related project contains the workings written in Python in testing the connectivity between MQTT, AMQP and Google Cloud Platform. 

Tag: Google Cloud PubSUb, paho mqtt, socketconnection

## ROS Dockey Car 
This project is to revamp a remote control car into an autonomous car by adding various control components. ROS is installed on the Raspbery PI as a controller to control the speed and steering angle with several ROS nodes. The motors received the PWM signal from Ardinuo and moves accordingly. These 2 majors eletronic components are communicated via ROSserial . 

Tag: ROS kinetic, rospy, Cmake


## Relational Database - Predictive Robot Maintenance System
This project is written in python for predicting the time for maintenance. The methodogly is to stored all simulated sensors values in a SQL Database and then perform data analysis base on a linear model. 

Tag: numpy, pandas, PyQt5


