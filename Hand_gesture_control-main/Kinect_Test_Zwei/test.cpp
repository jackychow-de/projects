/*
Copyright (C) 2010 Arne Bernin
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Modified Code
Copyright (C) 2013 Jay Rambhia
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Makefile for Ubuntu
CXXFLAGS = -O2 -g -Wall -fmessage-length=0 `pkg-config opencv --cflags ` -I /usr/include/libusb-1.0
OBJS = freenectopencvcv::Mat.o 
LIBS = `pkg-config opencv --libs` -lfreenect

TARGET = kinectopencv
$(TARGET):$(OBJS)
    $(CXX) -o $(TARGET) $(OBJS) $(LIBS)
all:$(TARGET)
clean:
    rm -f $(OBJS) $(TARGET)
*/


#include <iostream>
#include <libfreenect/libfreenect.h>
#include <pthread.h>
#define CV_NO_BACKWARD_COMPATIBILITY
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/video/background_segm.hpp"

#define FREENECTOPENCV_WINDOW_D "Depthimage"
#define FREENECTOPENCV_WINDOW_N "Normalimage"
#define FREENECTOPENCV_RGB_DEPTH 3
#define FREENECTOPENCV_DEPTH_DEPTH 1
#define FREENECTOPENCV_RGB_WIDTH 640
#define FREENECTOPENCV_RGB_HEIGHT 480
#define FREENECTOPENCV_DEPTH_WIDTH 640
#define FREENECTOPENCV_DEPTH_HEIGHT 480
#define FREENECT_VIDEO_RGB_SIZE    sizeof(uint8_t)*FREENECTOPENCV_RGB_WIDTH*FREENECTOPENCV_RGB_HEIGHT//*3


int set_bg = 0;
int gesture_cap = 0;


using namespace std;
using namespace cv;

pthread_t fnkt_thread;
pthread_mutex_t buf_mutex = PTHREAD_MUTEX_INITIALIZER;

cv::Mat depth_img, depth_raw, canny_temp, bkgimg, temp_img, mask, mask_fg, mask_bg, frame,fgMask, element ;

 
void depth_cb(freenect_device *dev, void* depth, uint32_t timestamp) {
		pthread_mutex_lock(&buf_mutex);
        cv::Mat d8;

        cv::Mat temp = cv::Mat (480,640, CV_16UC1, depth);

        temp.convertTo(d8, CV_8UC1, -255.0f/4000, 255.0f);

		// copy to OpenCV image buffer
		memcpy(depth_raw.data, d8.data, 640*480);
 
		pthread_mutex_unlock(&buf_mutex);
}

int main()
{
    depth_raw = cv::Mat (480,640, CV_8UC1);
    depth_img = cv::Mat (480,640, CV_8UC1);
    temp_img = cv::Mat (480,640, CV_8UC1);
    mask = cv::Mat (480,640, CV_8UC1);
    fgMask = cv::Mat (480,640, CV_8UC1);


    frame = cv::Mat(480,640,CV_8UC1, 255);
    frame(cv::Rect(30, 40, 565, 440)) = 0;

    int erosion_size = 10;
    element = cv::getStructuringElement(
        0, cv::Size(3 * erosion_size + 1, erosion_size + 1),
        cv::Point(erosion_size, erosion_size));

/*
    Ptr<BackgroundSubtractor> pBackSub;
    pBackSub = cv::createBackgroundSubtractorMOG2();
*/

    freenect_context *ctx;
    freenect_device *dev;

    if (freenect_init(&ctx, NULL) < 0)
    {
        std::cout <<" Fail to start " << std::endl;
        return 1;

    }

    if (freenect_open_device(ctx , &dev, 0) < 0)
    {
        std::cout <<"Fail to connect Kinect"<< std::endl;
        return 1;
    }

    freenect_set_depth_callback(dev,depth_cb);

    freenect_start_depth(dev);
       

    while(freenect_process_events(ctx) >= 0)
    {   

        depth_raw.copyTo(depth_img);

        if (set_bg) {
			printf("resetting background...\n");
			            
            cv::GaussianBlur(depth_img, temp_img, cv::Size(21, 21), 0);

            // Set depth image to binary values: foreground and background
            cv::threshold(temp_img, temp_img, 8888,
                        FREENECT_DEPTH_MM_MAX_VALUE, cv::THRESH_BINARY);


            // Set to binary greyscale
            temp_img.convertTo(mask, CV_8UC1, 255.0 / FREENECT_DEPTH_MM_MAX_VALUE);

            // Fix curved edges
            //mask = mask | frame;

            // Dilate to eliminate gaps in background and decrease the border around
            // the foreground object/person
            //cv::dilate(mask, mask, element);

            //cv::cvtColor(mask, mask, cv::COLOR_GRAY2BGR);

            // Generate foreground image
            //mask_fg = mask.clone();
           // mask_fg = ~mask_fg;
            //final_fg = mask_fg;

         //   mask_bg = mask;

 			set_bg--;
		    }

           
     cv::imshow("Depth_raw", depth_raw); 
     cv::imshow("Depth_img", depth_img); 
     cv::imshow("Mask", mask); 
     //cv::imshow("Mask_fg",  mask_fg); 


       /*
        //cv::imshow("Depth", img);
        cv::Canny(img, canny_temp, 50.0, 200.0, 3);      
        cv::imshow("Depth Canny", canny_temp);
        
         //update the background model
        pBackSub->apply(canny_temp, fgMask);
        cv::imshow("FG Mask", fgMask);



        if (gesture_cap) {
			printf("resetting background...\n");
			canny_temp.copyTo( rawimg );
            // subtract current image from background  (not working)
            //cv::absdiff(bkgimg,rawimg ,diffimg );// Absolute differences between the 2 images (not working)
		    //cv::threshold(diffimg ,diffimg ,15,255,cv::THRESH_BINARY); (not working)
           // cv::subtract( bkgimg, rawimg, diffimg );
 			gesture_cap--;
		    }
  

        //cv::imshow("Background Image",  bkgimg); 
		//cv::imshow("Gesture Capture",  diffimg); 
        */

        char command = cv::waitKey(1);
		if ( command == 'q' ) {std::cout<<FREENECT_DEPTH_MM_MAX_VALUE<<std::endl; break;}  // ESC key 27
		if ( command == 32 ) set_bg = 1; // space
        if ( command == 'c' ) gesture_cap = 1; // c

    }   
}