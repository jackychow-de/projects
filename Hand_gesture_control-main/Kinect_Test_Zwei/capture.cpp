/*
Copyright (C) 2010 Arne Bernin
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Modified Code
Copyright (C) 2013 Jay Rambhia
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Makefile for Ubuntu
CXXFLAGS = -O2 -g -Wall -fmessage-length=0 `pkg-config opencv --cflags ` -I /usr/include/libusb-1.0
OBJS = freenectopencvcv::Mat.o 
LIBS = `pkg-config opencv --libs` -lfreenect

TARGET = kinectopencv
$(TARGET):$(OBJS)
    $(CXX) -o $(TARGET) $(OBJS) $(LIBS)
all:$(TARGET)
clean:
    rm -f $(OBJS) $(TARGET)
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libfreenect/libfreenect.h>
#include <pthread.h>
#define CV_NO_BACKWARD_COMPATIBILITY
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv_modules.hpp"

#define FREENECTOPENCV_WINDOW_D "Depthimage"
#define FREENECTOPENCV_WINDOW_N "Normalimage"
#define FREENECTOPENCV_RGB_DEPTH 3
#define FREENECTOPENCV_DEPTH_DEPTH 1
#define FREENECTOPENCV_RGB_WIDTH 640
#define FREENECTOPENCV_RGB_HEIGHT 480
#define FREENECTOPENCV_DEPTH_WIDTH 640
#define FREENECTOPENCV_DEPTH_HEIGHT 480
#define FREENECT_VIDEO_RGB_SIZE    sizeof(uint8_t)*FREENECTOPENCV_RGB_WIDTH*FREENECTOPENCV_RGB_HEIGHT//*3

volatile int die = 0;
pthread_mutex_t mid_buffer_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t freenect_thread;

//using namespace cv;
using namespace std;

cv::Mat depthimg, rgbimg, tempimg, canny_temp, canny_img;

pthread_mutex_t mutex_depth = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_rgb = PTHREAD_MUTEX_INITIALIZER;
pthread_t cv_thread;
 
freenect_context *f_ctx;
freenect_device *f_dev;
//freenect_frame_mode video_mode = freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB);
freenect_frame_mode depth_mode = freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_11BIT);

cv::Mat1s *depth_back, *depth_mid, *depth_front; //Depth image buffers
//cv::Mat3b *rgb_back, *rgb_mid, *rgb_front;        //Color image buffers
int got_depth=0, got_rgb=0;

const float calib_fx_d=586.16f; //These constants come from calibration,
const float calib_fy_d=582.73f; //replace with your own
const float calib_px_d=322.30f;
const float calib_py_d=230.07;
const float calib_dc1=-0.002851;
const float calib_dc2=1093.57;
const float calib_fx_rgb=530.11f;
const float calib_fy_rgb=526.85f;
const float calib_px_rgb=311.23f;
const float calib_py_rgb=256.89f;
cv::Matx33f calib_R( 0.99999,   -0.0021409,     0.004993,
                    0.0022251,      0.99985,    -0.016911,
                    -0.0049561,     0.016922,      0.99984);
cv::Matx31f calib_T(  -0.025985,   0.00073534,    -0.003411);

void depth_cb(freenect_device *dev, void *depth, uint32_t timestamp) {
    assert(depth == depth_back->data);    
    pthread_mutex_lock(&mid_buffer_mutex);

    //Swap buffers
    cv::Mat1s *temp = depth_back;
    depth_back = depth_mid;
    depth_mid = temp;

    freenect_set_depth_buffer(dev, depth_back->data);
    got_depth++;
    
    pthread_mutex_unlock(&mid_buffer_mutex);
}

void *freenect_threadfunc(void *arg) {
    //Init freenect
    if (freenect_init(&f_ctx, NULL) < 0) {
        cout << "freenect_init() failed\n";
        die = 1;
        return NULL;
    }
    freenect_set_log_level(f_ctx, FREENECT_LOG_WARNING);

    int nr_devices = freenect_num_devices (f_ctx);
    cout << "Number of devices found: " << nr_devices << endl;

    if (nr_devices < 1) {
        die = 1;
        return NULL;
    }

    if (freenect_open_device(f_ctx, &f_dev, 0) < 0) {
        cout << "Could not open device\n";
        die = 1;
        return NULL;
    }

    freenect_set_led(f_dev,LED_GREEN);

    freenect_set_depth_callback(f_dev, depth_cb);
    freenect_set_depth_mode(f_dev, depth_mode);
    freenect_set_depth_buffer(f_dev, depth_back->data);    
    /*
    freenect_set_video_callback(f_dev, rgb_cb);
    freenect_set_video_mode(f_dev, video_mode);
    freenect_set_video_buffer(f_dev, rgb_back->data);
    */
    freenect_start_depth(f_dev);
    //freenect_start_video(f_dev);
    
  

    while (!die && freenect_process_events(f_ctx) >= 0) {
        //Let freenect process events

    }

    

    cout << "Shutting down Kinect...";
    

    freenect_stop_depth(f_dev);
    //freenect_stop_video(f_dev);

    freenect_close_device(f_dev);
    freenect_shutdown(f_ctx);

    cout << "done!\n";
    return NULL;
}



int main(int argc, char **argv){
    if (freenect_init(&f_ctx, NULL) < 0) {
        printf("freenect_init() failed\n");
        return 1;
    }

 
    freenect_set_led(f_dev,);
    freenect_set_depth_callback(f_dev, depth_cb);
    //freenect_set_video_callback(f_dev, KinectFreenect::rgb_cb);
    //freenect_set_video_mode(f_dev, freenect_find_video_mode(current_resolution, current_format));
    freenect_set_depth_mode(f_dev, freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_11BIT));

    freenect_start_depth(f_dev);
   // freenect_start_video(f_dev);
    
    int res;
    res = pthread_create(&freenect_thread, NULL, freenect_threadfunc, NULL);
    if (res) {
        printf("pthread_create failed\n");
        return 1;
    }

    int status = 0;
    while (!die && status >= 0) {
        char k = cv::waitKey(5);
        if( k == 27 ) {
            die = 1;
            break;
        }
    }


    return 0;
}