/*
Copyright (C) 2010 Arne Bernin
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Modified Code
Copyright (C) 2013 Jay Rambhia
This code is licensed to you under the terms of the GNU GPL, version 2 or version 3;
see:
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
http://www.gnu.org/licenses/gpl-3.0.txt
*/
/*
Makefile for Ubuntu
CXXFLAGS = -O2 -g -Wall -fmessage-length=0 `pkg-config opencv --cflags ` -I /usr/include/libusb-1.0
OBJS = freenectopencvcv::Mat.o 
LIBS = `pkg-config opencv --libs` -lfreenect

TARGET = kinectopencv
$(TARGET):$(OBJS)
    $(CXX) -o $(TARGET) $(OBJS) $(LIBS)
all:$(TARGET)
clean:
    rm -f $(OBJS) $(TARGET)
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libfreenect/libfreenect.h>
#include <pthread.h>
#define CV_NO_BACKWARD_COMPATIBILITY
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/opencv_modules.hpp"
#include <mutex>
#include <vector> 

#define FREENECTOPENCV_WINDOW_D "Depthimage"
#define FREENECTOPENCV_WINDOW_N "Normalimage"
#define FREENECTOPENCV_RGB_DEPTH 3
#define FREENECTOPENCV_DEPTH_DEPTH 1
#define FREENECTOPENCV_RGB_WIDTH 640
#define FREENECTOPENCV_RGB_HEIGHT 480
#define FREENECTOPENCV_DEPTH_WIDTH 640
#define FREENECTOPENCV_DEPTH_HEIGHT 480
#define FREENECT_VIDEO_RGB_SIZE    sizeof(uint8_t)*FREENECTOPENCV_RGB_WIDTH*FREENECTOPENCV_RGB_HEIGHT//*3


//using namespace cv;
using namespace std;

vector<vector<cv::Point>> contours;
vector<cv::Vec4i> hierarchy;
vector<cv::Point>  cont;

bool die = false;
int reset_bg = 5;
int find_contour = 5;

cv::Mat depthimg, rgbimg, blur_img, bkgimg, thr,dst ,depthMat;

pthread_mutex_t mutex_depth = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_rgb = PTHREAD_MUTEX_INITIALIZER;
pthread_t cv_thread;
 
//
bool getDepth(cv::Mat& output);
bool m_new_depth_frame;

std::mutex m_depth_mutex;
std::mutex new_dep_frame_mutex;



// callback for depthimage, called by libfreenect
void depth_cb(freenect_device *dev, void *depth, uint32_t timestamp)
 
{
    cv::Mat depth8;
    cv::Mat mydepth = cv::Mat( FREENECTOPENCV_DEPTH_WIDTH,FREENECTOPENCV_DEPTH_HEIGHT, CV_16UC1, depth);

    mydepth.convertTo(depth8, CV_8UC1, 1.0/4.0);
    pthread_mutex_lock( &mutex_depth );
    memcpy(depthimg.data, depth8.data, 640*480);
    // unlock mutex
    pthread_mutex_unlock( &mutex_depth );
 
}
 
bool getDepth(cv::Mat& output) {
  m_depth_mutex.lock();

  if (m_new_depth_frame) {
    depthMat.copyTo(output);
    m_new_depth_frame = false;
    m_depth_mutex.unlock();
    return true;

  } else {
    m_depth_mutex.unlock();
    return false;
  }

}
// callback for rgbimage, called by libfreenect
 
void rgb_cb(freenect_device *dev, void *rgb, uint32_t timestamp)
{
 
    // lock mutex for opencv rgb image
    pthread_mutex_lock( &mutex_rgb );
    memcpy(rgbimg.data, rgb, FREENECT_VIDEO_RGB_SIZE);
    // unlock mutex
    pthread_mutex_unlock( &mutex_rgb );
}
 
/*
 * thread for displaying the opencv content
 */
  

void *cv_threadfunc (void *ptr) {
    int largest_area = 0;
    int largest_contour_index = 0;
    cv::Rect bounding_rect;

    thr = cv::Mat(FREENECTOPENCV_DEPTH_HEIGHT, FREENECTOPENCV_DEPTH_WIDTH, CV_8UC1);
    dst = cv::Mat(FREENECTOPENCV_DEPTH_HEIGHT, FREENECTOPENCV_DEPTH_WIDTH, CV_8UC1,cv::Scalar::all(0));
    depthimg = cv::Mat(FREENECTOPENCV_DEPTH_HEIGHT, FREENECTOPENCV_DEPTH_WIDTH, CV_8UC1);
    rgbimg = cv::Mat(FREENECTOPENCV_DEPTH_HEIGHT, FREENECTOPENCV_DEPTH_WIDTH, CV_8UC1);
    blur_img = cv::Mat(FREENECTOPENCV_RGB_HEIGHT, FREENECTOPENCV_RGB_WIDTH, CV_8UC1);
    bkgimg = cv::Mat(FREENECTOPENCV_DEPTH_HEIGHT, FREENECTOPENCV_DEPTH_WIDTH, CV_16UC1);
    
    // use image polling
    while (!die)
    {
        
        
        //lock mutex for depth image
        pthread_mutex_lock( &mutex_depth );

         	if (reset_bg) {
			printf("resetting background...\n");
			blur_img.copyTo( bkgimg );
 			reset_bg--;
			std::cout << reset_bg;
		    }

         cv::cvtColor(depthimg,thr,cv::COLOR_BGR2GRAY);
        cv::threshold(thr, thr, 25, 255, cv::THRESH_BINARY);
         
        cv::findContours(thr, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);
        cv::Scalar color(0,0,255);
     
    for(int i = 0; i < contours.size(); i++) // Iterate through each contour
    {
        double a = cv::contourArea(contours[i], false); // Find the area of contour
        if(a > largest_area){
            largest_area = a;
            largest_contour_index = i; // Store the index of largest contour
            bounding_rect = cv::boundingRect(contours[i]); // Find the bounding rectangle for biggest contour
        }
         
    }

    for(int i = 0; i < contours.size(); i++) // Iterate through each contour
    {
        if (i != largest_contour_index) {
            cv::drawContours(dst, contours, i, color, cv::FILLED, 8, hierarchy);
        }
    }
     
    cv::Scalar color2(255,0,0);
    cv::drawContours(dst, contours,largest_contour_index, color2, 5, 8, hierarchy); // Draw the largest contour using previously stored index.
     
    //cv::rectangle( depthimg, cv::boundingRect, cv::Scalar(0,255,0), 1, 8);
    cv::imshow("Happy Thread", thr);
    cv::imshow("Happy Largest Contour", dst);

    
        //unlock mutex for depth image
        pthread_mutex_unlock( &mutex_depth );
        
        
        // wait for quit key
       
        char k = cv::waitKey(15);

		if ( k == 27 ) {die = true;   break;}  // ESC key
		if ( k == 32 ) reset_bg = 1; // space
        if ( k == 8 ) find_contour = 1; // backspace
     
    }
    pthread_exit(NULL);

    return NULL;
}


int main(int argc, char **argv)
{   
    
 
    freenect_context *f_ctx;
    freenect_device *f_dev;
 
    int res = 0;
    int die = 0;
    printf("Kinect camera test\n");
 
    if (freenect_init(&f_ctx, NULL) < 0)
    {
        printf("freenect_init() failed\n");
        return 1;
    }
 
    if (freenect_open_device(f_ctx, &f_dev, 0) < 0)
    {
        printf("Could not open device\n");
        return 1;
    }
 

    freenect_set_depth_callback(f_dev, depth_cb);
 
    // create opencv display thread
    res = pthread_create(&cv_thread, NULL, cv_threadfunc, NULL);
    if (res)
    {
        printf("pthread_create failed\n");
        return 1;
    }
    printf("init done\n");
 
    freenect_start_depth(f_dev);
 
    while(!die && freenect_process_events(f_ctx) >= 0 );
    

}