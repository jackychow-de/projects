# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('C:\\Users\\ARX-R01J\\Desktop\\Maching Learning\\Project 01\\Development\\Data Set\\Sensor_Data2.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1 / 3, random_state=0)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)


# Predicting the Test set results
y_pred = regressor.predict(X_test)


# Visualising the Training set results
plt.scatter(X_train, y_train, color = 'red')
plt.plot(X_train, regressor.predict(X_train), color = 'blue')
plt.title('Sensor vs Power (Training set)')
plt.xlabel('Power error')
plt.ylabel('Sensor error')
print(regressor.predict(X_test))
print(regressor.score(X_test,y_test))
print(len(X_train))
plt.show()
print('hi')


#C:\Users\ARX-R01J\AppData\Local\Programs\Python\Python36-32\Scripts>pip --version
