# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Query.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(843, 696)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton1 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton1.setGeometry(QtCore.QRect(80, 120, 75, 23))
        self.pushButton1.setObjectName("pushButton1")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(220, 120, 113, 20))
        self.lineEdit.setObjectName("lineEdit")
        self.label1 = QtWidgets.QLabel(self.centralwidget)
        self.label1.setGeometry(QtCore.QRect(480, 120, 47, 13))
        self.label1.setObjectName("label1")
        self.tableWidget1 = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget1.setGeometry(QtCore.QRect(40, 190, 651, 321))
        self.tableWidget1.setRowCount(10)
        self.tableWidget1.setColumnCount(6)
        self.tableWidget1.setObjectName("tableWidget1")
        self.LOADQ_BTN1 = QtWidgets.QPushButton(self.centralwidget)
        self.LOADQ_BTN1.setGeometry(QtCore.QRect(40, 540, 75, 23))
        self.LOADQ_BTN1.setObjectName("LOADQ_BTN1")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 843, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton1.setText(_translate("MainWindow", "OK"))
        self.label1.setText(_translate("MainWindow", "TextLabel"))
        self.LOADQ_BTN1.setText(_translate("MainWindow", "Load"))

