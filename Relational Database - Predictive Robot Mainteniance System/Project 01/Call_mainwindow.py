import sys
import mysql.connector
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow, QApplication
from SensorQuery import *


class MyMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MyMainWindow, self).__init__(parent)

        self.setupUi(self)
        self.setWindowTitle("Query")
        self.tableWidget1.setRowCount(0)
        self.pushButton1.clicked.connect(self.on_pushbutton_clicked)
        self.LOADQ_BTN1.clicked.connect(self.loaddata)

    @pyqtSlot()
    def on_pushbutton_clicked(self):
        self.label1.setText("12345")

    def loaddata(self):
        #Show Table Headings
        _translate = QtCore.QCoreApplication.translate
        item = self.tableWidget1.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "RecordNo"))
        item = self.tableWidget1.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "ReadingTimeStamp"))
        item = self.tableWidget1.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "SensorActReading"))
        item = self.tableWidget1.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "SensorRefReading"))
        item = self.tableWidget1.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "SensorAccError"))
        item = self.tableWidget1.horizontalHeaderItem(5)
        item.setText(_translate("MainWindow", "SensorID"))

        #DB Connection
        mydb = mysql.connector.connect(
            host="localhost",
            user="ARX_R09J",
            passwd="ROeng_913",
            database="robots_pdm"
        )
        mycursor = mydb.cursor()
        sql_statement = "SELECT * FROM sensor_readings"
        mycursor.execute(sql_statement)
        results = mycursor.fetchall()

        self.tableWidget1.setRowCount(0)
        for row_number, row_data in enumerate(results):
            self.tableWidget1.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget1.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = MyMainWindow()
    myWin.show()
    sys.exit(app.exec_())