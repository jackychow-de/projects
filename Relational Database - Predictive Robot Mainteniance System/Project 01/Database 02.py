
import csv
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="ARX_R09J",
  passwd="ROeng_913",
  database="robots_pdm"
)

print(mydb)

mycursor = mydb.cursor()


# Open CSV file beware of the path use / instead of \
with open('C:/Users/ARX-R01J/Desktop/Sensor1001.csv', newline='') as csvfile:
  reader = csv.DictReader(csvfile)
  for row in reader:
    print(row['RecordNo'], row['ReadingTimeStamp'], row['SensorActReading'], row['SensorRefReading'], \
          row['SensorAccError'], row['SensorID'])
    # insert
    conn = mydb
    sql_statement = "INSERT INTO sensor_readings( \
         RecordNo,ReadingTimeStamp,SensorActReading,SensorRefReading,SensorAccError,SensorID)\
         VALUES (%s,%s,%s,%s,%s,%s)"
    cur = conn.cursor()
    cur.executemany(sql_statement, [(row['RecordNo'], row['ReadingTimeStamp'], row['SensorActReading'], \
                                     row['SensorRefReading'], row['SensorAccError'], row['SensorID'])])
    #conn.escape_string(sql_statement)
    conn.commit()